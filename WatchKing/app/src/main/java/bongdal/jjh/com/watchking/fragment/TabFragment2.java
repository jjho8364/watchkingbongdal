package bongdal.jjh.com.watchking.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import bongdal.jjh.com.watchking.R;
import bongdal.jjh.com.watchking.adapter.Fr2ListViewAdapter;
import bongdal.jjh.com.watchking.items.Fr2ListViewItem;

/**
 * Created by Administrator on 2016-06-18.
 */
public class TabFragment2 extends Fragment {
    private View view = null;
    private TextView tv;
    private ListView listview;
    private ArrayList<Fr2ListViewItem> listArr;

    private ProgressDialog mProgressDialog;
    //private String baseUrl = "https://www.youtube.com/user/BongDalTV/videos";



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.tab_fragment_2, container, false);
        super.onCreate(savedInstanceState);

        // 초기화
        listArr = new ArrayList<Fr2ListViewItem>();

        listview = (ListView)view.findViewById(R.id.fr2_listview);
        //listview.setAdapter(new Fr2ListViewAdapter(getActivity().getLayoutInflater(), listArr));

        new getFr2ListView().execute();

        return view;
    }


    public class getFr2ListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            listArr = null;
            listArr = new ArrayList<Fr2ListViewItem>();
            String location = "https://m.youtube.com/user/BongDalTV/videos";
            Document doc = null;
            try{
                doc = Jsoup.connect(location)
                        .userAgent("Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1")
                        .get();
                Elements ul = doc.select("div._meb._mtf");

                //for(int i=0 ; i<30 ; i++){
                    //Elements li = ul.select("li:nth-child(" + (i + 1) + ")");
                    Elements li = ul.select("div:nth-child(1)");
                    String imgSrc = li.select("img ._mut").attr("src");
                    String title = li.select("span ._mgbb sapn:nth-child(1)").text();
                    String checkCnt = li.select("span ._mgbb sapn:nth-child(2)").text();
                    String dateUpload = "";

                    Fr2ListViewItem tempItem = new Fr2ListViewItem(imgSrc, title, checkCnt, dateUpload);

                    listArr.add(tempItem);
                //}


            } catch(Exception e){

            }




            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listview.setAdapter(new Fr2ListViewAdapter(getActivity().getLayoutInflater(), listArr));

            mProgressDialog.dismiss();


        }
    }

}
