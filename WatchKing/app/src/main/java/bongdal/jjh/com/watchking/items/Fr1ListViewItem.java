package bongdal.jjh.com.watchking.items;

/**
 * Created by Administrator on 2016-06-19.
 */
public class Fr1ListViewItem {
    int image;
    private String id;
    private String name;
    private String belong;
    private String line;
    private String url;

    public Fr1ListViewItem(int image, String id, String name, String belong, String line, String url) {
        this.image = image;
        this.id = id;
        this.name = name;
        this.belong = belong;
        this.line = line;
        this.url = url;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBelong() {
        return belong;
    }

    public void setBelong(String belong) {
        this.belong = belong;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }
}
