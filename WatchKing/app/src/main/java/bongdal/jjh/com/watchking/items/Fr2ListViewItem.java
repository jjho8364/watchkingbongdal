package bongdal.jjh.com.watchking.items;

/**
 * Created by Administrator on 2016-06-19.
 */
public class Fr2ListViewItem {
    String imgUrl;
    String title;
    String checkCnt;
    String date_upload;

    public Fr2ListViewItem(String imgUrl, String title, String checkCnt, String date_upload) {
        this.imgUrl = imgUrl;
        this.title = title;
        this.checkCnt = checkCnt;
        this.date_upload = date_upload;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCheckCnt() {
        return checkCnt;
    }

    public void setCheckCnt(String checkCnt) {
        this.checkCnt = checkCnt;
    }

    public String getDate_upload() {
        return date_upload;
    }

    public void setDate_upload(String date_upload) {
        this.date_upload = date_upload;
    }
}
