package bongdal.jjh.com.watchking.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import bongdal.jjh.com.watchking.R;
import bongdal.jjh.com.watchking.activity.MainActivity;
import bongdal.jjh.com.watchking.adapter.Fr1ListViewAdapter;
import bongdal.jjh.com.watchking.items.Fr1ListViewItem;

/**
 * Created by Administrator on 2016-06-18.
 */
public class TabFragment1 extends Fragment {
    private View view = null;
    private TextView tv;
    private ListView listview;  // 리스트 뷰
    private ArrayList<Fr1ListViewItem> listArr; //  리스트뷰 item 리스트


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.tab_fragment_1, container, false);
        super.onCreate(savedInstanceState);

        // 초기화
        listArr = new ArrayList<Fr1ListViewItem>();
        /////////////
        listArr.add(new Fr1ListViewItem(R.drawable.bongdal, "BJ봉달", "봉달이","faker 후배","관전 31000판 해설", "https://www.youtube.com/user/BongDalTV/videos"));
        listArr.add(new Fr1ListViewItem(R.drawable.sk_mid, "FAKER", "이상혁","SKT T1","미드", ""));
        listArr.add(new Fr1ListViewItem(R.drawable.sk_top, "DUKE", "이호성","SKT T1","탑", ""));
        listArr.add(new Fr1ListViewItem(R.drawable.sk_jungle, "BENGI", "배성웅","SKT T1","정글", ""));
        listArr.add(new Fr1ListViewItem(R.drawable.sk_ad, "BANG", "배준식","SKT T1","원딜", ""));
        listArr.add(new Fr1ListViewItem(R.drawable.sk_sup, "WOLF", "이재완","SKT T1","서포터", ""));
        ////////////

        listview = (ListView)view.findViewById(R.id.fr1_listview);
        listview.setAdapter(new Fr1ListViewAdapter(getActivity(), getActivity().getLayoutInflater(), listArr));



        return view;
    }
}
