package bongdal.jjh.com.watchking.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import bongdal.jjh.com.watchking.R;
import bongdal.jjh.com.watchking.activity.MainActivity;
import bongdal.jjh.com.watchking.items.Fr1ListViewItem;

/**
 * Created by Administrator on 2016-06-19.
 */

public class Fr1ListViewAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Fr1ListViewItem> listViewItem;

    public Fr1ListViewAdapter(Context context, LayoutInflater inflater, ArrayList<Fr1ListViewItem> listViewItem) {
        this.context = context;
        this.inflater = inflater;
        this.listViewItem = listViewItem;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.fr1_listview_item, null);
        if(convertView != null){
            LinearLayout linearLayout = (LinearLayout)convertView.findViewById(R.id.fr1_listitem_replay);
            ImageView img = (ImageView)convertView.findViewById(R.id.fr1_listitem_img);
            TextView tv_id = (TextView)convertView.findViewById(R.id.fr1_listitem_id);
            TextView tv_name = (TextView)convertView.findViewById(R.id.fr1_listitem_name);
            TextView tv_belong = (TextView)convertView.findViewById(R.id.fr1_listitem_belong);
            TextView tv_position = (TextView)convertView.findViewById(R.id.fr1_listitem_position);

            img.setImageResource(listViewItem.get(position).getImage());
            tv_id.setText(listViewItem.get(position).getId());
            tv_name.setText(listViewItem.get(position).getName());
            tv_belong.setText(listViewItem.get(position).getBelong());
            tv_position.setText(listViewItem.get(position).getLine());

            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 페이지 이동
                    MainActivity.viewPager.setCurrentItem(MainActivity.viewPager.getCurrentItem()+1, true);

                }
            });
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return listViewItem.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}
