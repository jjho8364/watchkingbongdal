package bongdal.jjh.com.watchking.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import bongdal.jjh.com.watchking.R;
import bongdal.jjh.com.watchking.items.Fr2ListViewItem;

/**
 * Created by Administrator on 2016-06-19.
 */
public class Fr2ListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<Fr2ListViewItem> listViewItem;

    public Fr2ListViewAdapter(LayoutInflater inflater, ArrayList<Fr2ListViewItem> listViewItem) {
        this.inflater = inflater;
        this.listViewItem = listViewItem;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.fr2_listview_item, null);

        if(convertView != null){
            ImageView imageView = (ImageView)convertView.findViewById(R.id.fr2_listview_image);
            TextView tv_title = (TextView)convertView.findViewById(R.id.fr2_listview_title);
            TextView tv_checkCnt = (TextView)convertView.findViewById(R.id.fr2_listview_checkcnt);
            TextView tv_dateUpload = (TextView)convertView.findViewById(R.id.fr2_listview_dateupload);

            Fr2ListViewItem data = listViewItem.get(position);
            Log.d("  data : ",data.getTitle());
            Glide.with(convertView.getContext()).load(data.getImgUrl()).into(imageView);
            tv_title.setText(data.getTitle());
            tv_checkCnt.setText(data.getCheckCnt());
            tv_dateUpload.setText(data.getDate_upload());
        } else {

        }



        return convertView;
    }

    @Override
    public int getCount() {
        return listViewItem.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}
